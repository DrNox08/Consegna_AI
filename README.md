# [Sim Project] for Event Horizon School



This project was created for educational purposes.
The scope was to create a Behavior Tree in Unity (using C#) to develop the behavior of an entity that performs tasks completely autonomously.
The project includes:

- A day/night cycle
- Daytime tasks (Working, Eating, Drinking)
- Nighttime tasks (Sleeping)
- UI elements for feedback

The prioritization of tasks is a personal choice and can be easily modified.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour, IMushroom
{
    [SerializeField] GameObject mushroom;
    


    public Transform Transform => transform;

    public void Consume()
    {
        mushroom.SetActive(false);
    }

    public void Respawn()
    {
        mushroom.SetActive(true);
    }
}

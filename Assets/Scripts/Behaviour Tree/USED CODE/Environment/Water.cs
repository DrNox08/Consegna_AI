using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour, IConsumable
{
    int amountToConsume = 0;
    bool hasBeenInteracted;
    float timeToConsumeOne = 0.15f;
    bool hasBeenCompleted;
    
    public int AmountToConsume { get => amountToConsume; set => amountToConsume = value; }
    public bool HasBeenInteracted { get => hasBeenInteracted; set => hasBeenInteracted = value; }
    public bool HasBeenCompleted { get => hasBeenCompleted; set => hasBeenCompleted = value; }
    public Transform Transform => throw new System.NotImplementedException();


    public void Consume()
    {
        if (!hasBeenInteracted)
        {
            StartCoroutine(DrinkSip());
            hasBeenInteracted = true;
        }
    }

    public void Respawn()
    {
        throw new System.NotImplementedException();
    }

    IEnumerator DrinkSip()
    {
        for (int i = 0; i < amountToConsume; i++)
        {
            yield return new WaitForSeconds(timeToConsumeOne);
            UI_Manager.OnHealthChanged(0, 25);
        }
        hasBeenCompleted = true;
        hasBeenInteracted = false;
    }
}


    







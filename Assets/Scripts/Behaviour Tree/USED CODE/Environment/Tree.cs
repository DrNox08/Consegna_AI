using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour, IInteractable
{
    [SerializeField] GameObject particles;
    [SerializeField] GameObject wholeTree;

    float timeToCut = 2;
    bool hasInteraced;
    bool hasBeenCut;

    public float TimeToComplete { get => timeToCut; }

    public bool HasBeenCompleted => hasBeenCut;

    private void Start()
    {
        particles.SetActive(false);
    }

    public void Interact()
    {
        if (!hasInteraced)
        {
            StartCoroutine(GetCut(timeToCut));
            hasInteraced = true;
        }
    }

    IEnumerator GetCut(float time)
    {
        particles.SetActive(true);
        yield return new WaitForSeconds(time);
        particles.SetActive(false);
        wholeTree.SetActive(false);
        hasBeenCut = true;
    }

    public void Respawn()
    {
        wholeTree.SetActive(true);
        particles.SetActive(false);
        hasInteraced = false;
        hasBeenCut = false;
    }

}

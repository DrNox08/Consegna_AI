using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepModel : MonoBehaviour
{
    public static Action OnChange;

    [SerializeField] GameObject bedModel;

    private void OnEnable()
    {
        OnChange += ChangeVisibility;
    }
    private void OnDisable()
    {
        OnChange -= ChangeVisibility;
    }

    void ChangeVisibility()
    {
        if(bedModel.activeInHierarchy) bedModel.SetActive(false);
        else bedModel.SetActive(true);
    }
}

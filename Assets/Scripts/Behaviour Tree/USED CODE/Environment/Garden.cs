using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garden : MonoBehaviour, IConsumable
{
    List<IMushroom> consumables = new List<IMushroom>();
    List<IMushroom> totalFood;

    int amountToConsume;
    bool hasBeenCompleted;
    bool hasBeenInteracted;
    public int AmountToConsume { get => amountToConsume; set => amountToConsume = value; }
    public bool HasBeenCompleted { get => hasBeenCompleted; set => hasBeenCompleted = value; }
    public bool HasBeenInteracted { get => hasBeenInteracted; set => hasBeenInteracted = value; }
    public Transform Transform { get => transform;  }

    private void Awake()
    {
        consumables.AddRange(GetComponentsInChildren<IMushroom>());
        totalFood = new List<IMushroom>(consumables);
    }
        

    public void Consume()
    {
        if(amountToConsume >= consumables.Count) { amountToConsume = consumables.Count; }
        if (!hasBeenInteracted)
        {
            StartCoroutine(Counter());
            hasBeenInteracted = true;
        }
    }

    



    IEnumerator Counter()
    {
        List<IMushroom> consumablesToConsume = new List<IMushroom>(consumables);

        for (int i = 0; i < amountToConsume; i++)
        {
            yield return new WaitForSeconds(0.25f);
            UI_Manager.OnHealthChanged(25, 0);
            consumablesToConsume[i].Consume();
        }

        foreach (var consumable in consumablesToConsume.GetRange(0, amountToConsume))
        {
            consumables.Remove(consumable);
        }

        hasBeenCompleted = true;
        hasBeenInteracted = false;
    }

    public void Respawn()
    {
        foreach (var consumable in totalFood)
        {
            consumables.Add(consumable);
            consumable.Respawn();
        }
        Debug.LogWarning("ho respawnato");   
    }
}

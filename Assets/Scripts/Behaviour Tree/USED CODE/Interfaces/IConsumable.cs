using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IConsumable 
{
    public int AmountToConsume {  get; set; }
    public bool HasBeenCompleted { get; set; }
    public bool HasBeenInteracted { get; set; }
    public Transform Transform { get; }
    public void Consume();
    public void Respawn();


    
}

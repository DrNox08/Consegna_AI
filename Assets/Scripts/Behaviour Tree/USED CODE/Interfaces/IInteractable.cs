using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable 
{
    public float TimeToComplete { get; }

    bool HasBeenCompleted { get; }
    public void Interact();
    public void Respawn();
}

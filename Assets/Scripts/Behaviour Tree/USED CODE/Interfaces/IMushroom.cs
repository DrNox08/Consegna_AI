using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMushroom
{
    public void Consume();
    public void Respawn();
    public Transform Transform { get; }
}

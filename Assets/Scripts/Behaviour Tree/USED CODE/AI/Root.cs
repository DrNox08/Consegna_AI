using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Root : Node
{
    struct NodeLevel
    {
        public int level;
        public Node node;
    }
    public Root()
    {
        name = "Root";
    }

    public void PrintTree()
    {
        string showTree = string.Empty;
        Stack<NodeLevel> stack = new Stack<NodeLevel>();

        Node currentNode = this;
        stack.Push(new NodeLevel { level = 0, node = currentNode });

        while (stack.Count > 0)
        {
            NodeLevel nextNode = stack.Pop();
            showTree += new string('-', nextNode.level) + nextNode.node.name + "\n";

            for (int i = nextNode.node.children.Count - 1; i >= 0; i--)
            {
                stack.Push(new NodeLevel { level = nextNode.level + 1, node = nextNode.node.children[i] });
            }
        }

        Debug.Log(showTree);
    }

    public override Status Process()
    {
        return children[currentChild].Process();
        
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence : Node
{
    public Sequence(string _name)
    {
        name = _name;
    }

    public Sequence AddChildren(params Node[] _children)
    {
        children.AddRange(_children);
        return this;
    }

    public override Status Process()
    {
        Status childStatus = children[currentChild].Process();

        if (childStatus == Status.Running) return Status.Running;

        if (childStatus == Status.Failure)
        {
            currentChild = 0;
            return Status.Failure;
        }

        currentChild++;

        if (currentChild >= children.Count)
        {
            currentChild = 0;
            return Status.Success;
        }

        return Status.Running;
    }

}


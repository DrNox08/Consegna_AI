using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaf : Node
{
    public delegate Status Tick();
    public Tick Function;

    public Leaf(string _name, System.Action action) { name = _name; }

    public Leaf(string _name, Tick func)
    {
        name = _name;
        Function = func;
    }

    public override Status Process()
    {
        if (Function == null) return Status.Failure;

        return Function();
    }
}

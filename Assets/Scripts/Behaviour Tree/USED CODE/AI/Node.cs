using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node 
{
    public enum Status
    {
        Success, Running, Failure
    }

    public Status status;

    public List<Node> children = new();
    public int currentChild;
    public string name;

    public Node() { }

    public Node(string _name)
    {
        name = _name;
    }

    public void AddChild(Node node)
    {
        children.Add(node);
    }

    public virtual Status Process()
    {
        return children[currentChild].Process();
    }
}

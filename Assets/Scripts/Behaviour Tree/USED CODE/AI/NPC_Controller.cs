using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC_Controller : MonoBehaviour
{
    Root tree;
    Node.Status treeStatus = Node.Status.Running;
    NavMeshAgent agent;
    AnimationController anim;
    bool isDayTime;

    Transform nextDestination;
    IInteractable interactable;
    IConsumable consumable;

    public enum ActionState { Free, Busy }
    ActionState activityState;

    [Header("Health")]
    [SerializeField] float hunger;
    [SerializeField] float thirst;
    [SerializeField] float hungerThreshold;
    [SerializeField] float thirstThreshold;
    const float MaxHunger = 1000;
    const float MaxThirst = 1000;

    [Header("Parameters")]
    [SerializeField] float minDistanceToTarget;
    [SerializeField] LayerMask treesLayer;
    [SerializeField] LayerMask foodLayer;
    [SerializeField] LayerMask waterLayer;

    [Header("Mesh")]
    [SerializeField] GameObject model;
    [SerializeField] GameObject wood;

    [Header("Destination")]
    [SerializeField] Transform work;
    [SerializeField] Transform storage;
    [SerializeField] Transform sleep;
    [SerializeField] Transform eat;
    [SerializeField] Transform drink;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<AnimationController>();
    }

    private void OnEnable()
    {
        TimeManager.OnTimeChanged += CheckTime;
    }

    private void OnDisable()
    {
        TimeManager.OnTimeChanged -= CheckTime;
    }

    private void Start()
    {
        hunger = MaxHunger;
        thirst = MaxThirst;
        wood.SetActive(false);
        InitTree();
    }

    private void Update()
    {
        treeStatus = tree.Process();
    }

    #region Behaviour Tree
    void InitTree()
    {
        tree = new Root();
        var whatToDo = new Selector("What To Do");

        var workSequence = new Sequence("Work Routine")
            .AddChildren(
                new Leaf("Can it Work?", CanWork),
                new Leaf("Go To Work", GoToWork),
                new Leaf("Choose The Tree", ChooseTree),
                new Leaf("Cut The Tree", CutTree),
                new Leaf("Go To Storage", GoToTheStorage),
                new Leaf("Drop The Wood", DropWood)
            );

        var drinkSequence = new Sequence("Drink Sequence")
            .AddChildren(
                new Leaf("Is It Thirsty?", IsThirsty),
                new Leaf("Go To Drink", GoToDrink),
                new Leaf("Drink", () => Consume(Drink))
            );

        var eatSequence = new Sequence("Eat Sequence")
            .AddChildren(
                new Leaf("Is It Hungry?", IsHungry),
                new Leaf("Go To Eat", GoToEat),
                new Leaf("Eat", () => Consume(Eat))
            );

        var sleepSequence = new Sequence("Sleep Sequence")
            .AddChildren(
                new Leaf("Is It Sleepy?", IsSleepy),
                new Leaf("Go To Sleep", GoToSleep),
                new Leaf("Sleep", Sleep)
            );

        tree.AddChild(whatToDo);
        whatToDo.AddChildren(workSequence, drinkSequence, eatSequence, sleepSequence);

        tree.PrintTree();
        activityState = ActionState.Free;
    }
    #endregion

    #region Checks
    bool IsInNeeds() => hunger <= hungerThreshold || thirst <= thirstThreshold;

    Node.Status CanWork() => !IsInNeeds() && isDayTime ? Node.Status.Success : Node.Status.Failure;

    Node.Status IsThirsty() => thirst <= thirstThreshold ? Node.Status.Success : Node.Status.Failure;

    Node.Status IsHungry() => hunger <= hungerThreshold ? Node.Status.Success : Node.Status.Failure;

    Node.Status IsSleepy() => !IsInNeeds() && !isDayTime ? Node.Status.Success : Node.Status.Failure;

    void CheckTime(float value)
    {
        isDayTime = value >= 8 && value < 23;
    }
    #endregion

    #region Universal
    Node.Status GoToDestination(Transform targetPos)
    {
        float distance = Vector3.Distance(targetPos.position, transform.position);
        consumable = null;
        if (activityState == ActionState.Free)
        {
            anim.PlayAnimation(AnimationName.WalkChainsaw);
            agent.SetDestination(targetPos.position);
            activityState = ActionState.Busy;
        }
        else if (Vector3.Distance(agent.pathEndPosition, targetPos.position) >= minDistanceToTarget)
        {
            activityState = ActionState.Free;
            return Node.Status.Failure;
        }
        else if (distance <= minDistanceToTarget)
        {
            activityState = ActionState.Free;
            return Node.Status.Success;
        }
        return Node.Status.Running;
    }

    void IncreaseNeeds()
    {
        float randHunger = Random.Range(20f, 25f) * Time.deltaTime;
        float randThirst = Random.Range(30f, 50f) * Time.deltaTime;
        if (hunger > 0) hunger -= randHunger;
        if (thirst > 0) thirst -= randThirst;
        UI_Manager.OnHealthChanged?.Invoke(-randHunger, -randThirst);
    }
    #endregion

    #region Work
    public Node.Status GoToWork()
    {
        Debug.Log("Going to work");
        return GoToDestination(work);
    }

    public Node.Status ChooseTree()
    {
        Debug.Log("Choosing tree");
        if (interactable == null)
        {
            var trees = Physics.OverlapSphere(transform.position, 40, treesLayer);
            int randomTree = Random.Range(0, trees.Length);
            nextDestination = trees[randomTree].gameObject.transform;
            interactable ??= trees[randomTree].gameObject.GetComponentInParent<IInteractable>();
            return Node.Status.Running;
        }
        return GoToDestination(nextDestination.transform);
    }

    public Node.Status CutTree()
    {
        IncreaseNeeds();
        interactable.Interact();
        if (interactable.HasBeenCompleted)
        {
            interactable = null;
            return Node.Status.Success;
        }
        anim.PlayAnimation(AnimationName.Attack2);
        return Node.Status.Running;
    }

    public Node.Status GoToTheStorage()
    {
        wood.SetActive(true);
        IncreaseNeeds();
        return GoToDestination(storage);
    }

    public Node.Status DropWood()
    {
        anim.PlayAnimation(AnimationName.IdleChainsaw);
        if (wood.activeInHierarchy) wood.SetActive(false);
        activityState = ActionState.Free;
        return Node.Status.Success;
    }
    #endregion

    #region Drink & Eat
    public Node.Status GoToDrink()
    {
        Debug.Log("Going to drink");
        return GoToDestination(drink);
    }

    public Node.Status GoToEat()
    {
        Debug.Log("Going to eat");
        return GoToDestination(eat);
    }

    Node.Status Consume(System.Action onConsumeCompleted)
    {
        if (consumable != null && consumable.HasBeenCompleted)
        {
            consumable.HasBeenCompleted = false;
            onConsumeCompleted.Invoke();
            consumable = null;
            return Node.Status.Success;
        }
        if (consumable == null)
        {
            Debug.Log("Searching for consumable");
            var layer = onConsumeCompleted == Drink ? waterLayer : foodLayer;
            var objects = Physics.OverlapSphere(transform.position, 10, layer);
            consumable = objects[0].gameObject.GetComponent<IConsumable>();
        }
        if (consumable != null && !consumable.HasBeenInteracted)
        {
            var maxNeed = onConsumeCompleted == Drink ? MaxThirst - thirst : MaxHunger - hunger;
            var randomChoice = Random.Range(25, maxNeed);
            var recover = Mathf.Round(randomChoice / 25) * 25;
            var unitsToConsume = (int)recover / 25;
            consumable.AmountToConsume = unitsToConsume;
            consumable.Consume();
        }
        anim.PlayAnimation(AnimationName.IdleChainsaw);
        return Node.Status.Running;
    }

    void Drink()
    {
        thirst += consumable.AmountToConsume * 25;
    }

    void Eat()
    {
        hunger += consumable.AmountToConsume * 25;
    }
    #endregion

    #region Sleep
    public Node.Status GoToSleep()
    {
        Debug.Log("Going to sleep");
        return GoToDestination(sleep);
    }

    public Node.Status Sleep()
    {
        if (!isDayTime)
        {
            if (model.activeInHierarchy)
            {
                model.SetActive(false);
                SleepModel.OnChange?.Invoke();
            }
            return Node.Status.Running;
        }
        else if (isDayTime)
        {
            if (!model.activeInHierarchy)
            {
                model.SetActive(true);
                SleepModel.OnChange?.Invoke();
            }
            return Node.Status.Success;
        }
        return Node.Status.Running;
    }
    #endregion

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, 10);
    }
#endif
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class UI_Manager : MonoBehaviour
{
    //events
    public static Action<float, float> OnHealthChanged;
    


    //Refs
    [SerializeField] TMP_Text timeToShow;
    [SerializeField] Image hungerBar;
    [SerializeField] Image thirstBar;

   


    private void OnEnable()
    {
        TimeManager.OnTimeChanged += ShowTime;
        OnHealthChanged += UpdateHealth;
        
    }
    private void OnDisable()
    {
        TimeManager.OnTimeChanged -= ShowTime;
        OnHealthChanged -= UpdateHealth;
    }

    void ShowTime(float time) // it's show timeeee
    {
        int hours = Mathf.FloorToInt(time);
        int minutes = Mathf.FloorToInt((time - hours) * 60);

        if (minutes == 60)
        {
            minutes = 0;
            hours += 1;
        }
        
        timeToShow.text = string.Format("ORE: {0:00}:{1:00}", hours, minutes);
    }

        

    void UpdateHealth(float _hunger, float _thirst)
    {
        float hungerAmount = _hunger / 1000;
        float thirstAmount = _thirst / 1000;
        hungerBar.fillAmount += hungerAmount;
        thirstBar.fillAmount += thirstAmount;
        
    }
}


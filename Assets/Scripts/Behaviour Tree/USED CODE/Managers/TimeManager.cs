using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
      public float secondsPerDay = 120f; // 2 min total
    public float currentTime = 0f;
    public float timeMultiplier = 1f; // speed up in inspector
    public Light directionalLight;
    public Gradient lightColor;

    public delegate void TimeHandler(float time);
    public static event TimeHandler OnTimeChanged;

    private void Update()
    {
        TimeCycle();
    }

    void TimeCycle()
    {
        float increment = (24f / secondsPerDay) * Time.deltaTime * timeMultiplier;
        currentTime += increment;
        currentTime %= 24f;

        OnTimeChanged?.Invoke(currentTime);

        UpdateDirectionalLight(currentTime);
    }

    void UpdateDirectionalLight(float time)
    {
        
        float timeNormalized = time / 24f;

        
        directionalLight.transform.localRotation = Quaternion.Euler(new Vector3((timeNormalized * 360f) - 90f, 170f, 0));

        
        if (time < 5f || time > 21f) // Night time
        {
            directionalLight.intensity = 0.2f;
        }
        else if (time >= 5f && time < 7f)
        {
            directionalLight.intensity = Mathf.Lerp(0.2f, 1f, (time - 5f) / 2f); // Sunrise 5-7
        }
        else if (time >= 7f && time <= 19f)
        {
            directionalLight.intensity = 1f; // Day time 7-19
        }
        else if (time > 19f && time <= 21f)
        {
            directionalLight.intensity = Mathf.Lerp(1f, 0.2f, (time - 19f) / 2f); // Sunset 7-9
        }

        
        if (lightColor != null)
        {
            directionalLight.color = lightColor.Evaluate(timeNormalized);
        }
    }


}

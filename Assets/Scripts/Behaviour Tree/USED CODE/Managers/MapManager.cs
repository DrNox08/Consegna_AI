using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    

    [SerializeField] LayerMask foodLayer;
    [SerializeField] LayerMask forestLayer;
    [SerializeField] Transform overlap_forest;
    [SerializeField] Transform overlap_food;
    float forest_radius = 25;
    float food_radius = 6;

    float currentTime;
    bool hasRespawned = false;

    IConsumable garden;
    List<IInteractable> trees = new List<IInteractable>();

    private void Start()
    {
        TakeRSS();
    }

    private void OnEnable()
    {
        TimeManager.OnTimeChanged += CheckTime;
    }

    private void OnDisable()
    {
        TimeManager.OnTimeChanged -= CheckTime;
    }

    void TakeRSS()
    {
        var treesColliders = Physics.OverlapSphere(overlap_forest.position, forest_radius, forestLayer);
        foreach (var obj in treesColliders)
        {
            var singleTree = obj.transform.parent.GetComponent<IInteractable>();
            trees.Add(singleTree);
        }
        var gardenCollider = Physics.OverlapSphere(overlap_food.position, food_radius, foodLayer);
        garden = gardenCollider[0].gameObject.GetComponent<IConsumable>();
    }

    void CheckTime(float _time)
    {
        currentTime = _time;
        int hour = Mathf.FloorToInt(currentTime);

        if (hour == 7 && !hasRespawned)
        {
            RespawnRSS();
            hasRespawned = true;
        }
        else if (hour != 7)
        {
            hasRespawned = false;
        }
    }

    void RespawnRSS()
    {
        foreach (var obj in trees)
        {
            obj.Respawn();
        }

        garden.Respawn();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(overlap_forest.position, forest_radius);
        Gizmos.DrawWireSphere(overlap_food.position, food_radius);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC_V2 : MonoBehaviour
{
    Root tree;
    Node.Status treeStatus = Node.Status.Running;
    NavMeshAgent agent;
    AnimationController anim;
    bool isDayTime; // not a state
    bool isEating;
    bool isDrinking;
    bool isSleeping;


    Transform nextDestination;
    IInteractable interactable;

    [Header("Health")]
    [SerializeField] float hunger;
    [SerializeField] float thirst;
    [SerializeField] float hungerTreshold;
    [SerializeField] float thirstTreshold;
    float maxHunger = 1000;
    float maxThirst = 1000;
    [Space]
    [Header("Parameters")]
    [SerializeField] float minDistanceToTarget;
    [SerializeField] LayerMask treesLayer;
    [SerializeField] LayerMask foodLayer;
    [Header("Mesh")]
    [SerializeField] GameObject model;
    [SerializeField] GameObject wood;



    [Header("Test")]
    [SerializeField] Transform work;
    [SerializeField] Transform storage;
    [SerializeField] Transform sleep;
    [SerializeField] Transform eat;
    [SerializeField] Transform drink;
    [SerializeField] Transform bedPosition;



    enum ActionState { Free, Busy }
    ActionState ActivityState;

    
    private int mushroomsToEat = 0;
    private int mushroomsEaten = 0;
    private Collider[] availableFood;
    private float eatingTime = 0;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<AnimationController>();
    }

    private void OnEnable()
    {
        TimeManager.OnTimeChanged += CheckTime;
    }
    private void OnDisable()
    {
        TimeManager.OnTimeChanged -= CheckTime;
    }

    

    private void Start()
    {
        //Set Health
        hunger = 1000;
        thirst = 1000;

        wood.SetActive(false);

        //Build The Tree
        tree = new Root();
       
        Node goToWork = new Leaf("Go to work", GoToWork);
        Node goToEat = new Leaf("Go To Eat", GoToEat);
        Node goToDrink = new Leaf("Go To Drink", GoToDrink);
        Node goToSleep = new Leaf("Go To Sleep", GoToSleep);
        Node sleep = new Leaf("Sleep", Sleep);
        Node findTree = new Leaf("Find a tree", ChooseTree);
        Node cutTheTree = new Leaf("Cut The Tree", CutTree);
        Node eat = new Leaf("Eating", Eat);
        Node goToTheStorage = new Leaf("Going To The Storage", GoToTheStorage);
        Node drink = new Leaf("Drinking", Drink);
        Node dropWood = new Leaf("Dropping Wood", DropWood);
        Node workRoutine = new Sequence("Working Routine");
        Node eating = new Sequence("Eating Time");
        Node drinking = new Sequence("Drinking Time");
        Node sleeping = new Sequence("Sleeping Time");
        Node checkHunger = new Leaf("Is It Hungry?", IsItHungry);

        Node checkThirst = new Leaf("Is It Thirsty?", IsItThirsty);


        Node CanWork = new Decorator("Can It work?", CanItWork);
        Node isInNeed = new Decorator("Does It Need Something?", IsInNeeds);
        Node isItSleepy = new Decorator("Is It Sleepy?", IsItSleepy);


        Node WhatToDo = new Selector("What Next?");
        Node WhatIsTheNeed = new Selector("What Does It Need?");


        tree.AddChild(WhatToDo);
        WhatToDo.AddChild(CanWork);
        CanWork.AddChild(workRoutine);
        workRoutine.AddChild(goToWork);
        workRoutine.AddChild(findTree);
        workRoutine.AddChild(cutTheTree);
        workRoutine.AddChild(goToTheStorage);
        workRoutine.AddChild(dropWood);

        WhatToDo.AddChild(isInNeed);
        isInNeed.AddChild(WhatIsTheNeed);

        WhatIsTheNeed.AddChild(drinking);
        drinking.AddChild(checkThirst);
        drinking.AddChild(goToDrink);
        drinking.AddChild(drink);

        WhatIsTheNeed.AddChild(eating);
        eating.AddChild(checkHunger);
        eating.AddChild(goToEat);
        eating.AddChild(eat);

        WhatToDo.AddChild(isItSleepy);
        isItSleepy.AddChild(sleeping);
        sleeping.AddChild(goToSleep);
        sleeping.AddChild(sleep);


        ActivityState = ActionState.Free;

        tree.PrintTree();
    }
        














    private void Update()
    {
        Debug.Log(treeStatus);

        treeStatus = tree.Process();
        if (treeStatus == Node.Status.Failure) tree.currentChild = 0;
    }




    //_______________________FUNCTIONS__________________________

    bool CanItWork()
    {
        return IsInNeeds() == false && isDayTime && ActivityState == ActionState.Free;
    }
    
        
        
    void CheckTime(float value)
    {
        Debug.Log("� giorno------>"+isDayTime);
        if(value >= 8 && value < 23) isDayTime = true;
        else isDayTime = false;
    }
        

    //________Move Functions________\\
    #region Movement
    Node.Status GoToDestination(Vector3 targetPos)
    {
        float distanceToTarget = Vector3.Distance(targetPos, transform.position);



        agent.SetDestination(targetPos);

        anim.PlayAnimation(AnimationName.WalkChainsaw);


        if (distanceToTarget <= minDistanceToTarget)
        {
            return Node.Status.Success;
        }
        
        return Node.Status.Running;
    }


            


    public Node.Status GoToWork()
    {
        Debug.Log("Going to work");
        return GoToDestination(work.position);
    }
        


    public Node.Status GoToSleep()
    {
        Debug.Log("Going to sleep");
        

        return GoToDestination(sleep.position);
    }


    public Node.Status GoToEat()
    {
        Debug.Log("Going to eat");
        return GoToDestination(eat.position);
    }

    public Node.Status GoToDrink()
    {
        Debug.Log("Going to drink");
        return GoToDestination(drink.position);
    }


    #endregion


    //__________Actions-> Work Sequence__________\\
    #region Working
    public Node.Status ChooseTree()
    {
        ActivityState = ActionState.Busy;

        if (interactable == null)
        {
            var trees = Physics.OverlapSphere(transform.position, 40, treesLayer);
            int randomTree = Random.Range(0, trees.Length);
            nextDestination = trees[randomTree].gameObject.transform;
            interactable ??= trees[randomTree].gameObject.GetComponentInParent<IInteractable>();
            return Node.Status.Running;
        }

        return GoToDestination(nextDestination.transform.position);


    }

    public Node.Status CutTree()
    {

        IncreaseNeeds();
        interactable.Interact();
        if (interactable.HasBeenCompleted)
        {
            interactable = null;
            return Node.Status.Success;
        }

        else return Node.Status.Running;

    }


    public Node.Status GoToTheStorage()
    {
        wood.SetActive(true);
        IncreaseNeeds();
        
        return GoToDestination(storage.position);
    }

    public Node.Status DropWood()
    {
        if (wood.activeInHierarchy) wood.SetActive(false);
        ActivityState = ActionState.Free;
        return Node.Status.Success;
    }

    #endregion

    //__________Actions-> Sleep Sequence__________\\
    #region Sleeping

    public Node.Status Sleep()
    {
        SleepCheck();

        return Node.Status.Success;
    }

    void SleepCheck()
    {
        if (!isSleeping && !isDayTime)
        {
            isSleeping = true;
            model.SetActive(false);
            SleepModel.OnChange();
        }
        else if (isSleeping && isDayTime)
        {
            model.SetActive(true);
            SleepModel.OnChange();
            isSleeping = false;
        }
    }

    #endregion







    //________Health Functions________\\
    #region Health
    bool IsInNeeds()
    {
        bool realNeed = hunger <= hungerTreshold || thirst <= thirstTreshold; // || isEating || isDrinking;
        if (realNeed && ActivityState == ActionState.Free) return true;
        if (realNeed && ActivityState == ActionState.Busy) return false;
        return realNeed;
    }

    bool IsItSleepy()
    {
        return !isDayTime && ActivityState == ActionState.Free;
    }
    public Node.Status IsItThirsty()
    {
        if( thirst <= thirstTreshold && ActivityState == ActionState.Free) return Node.Status.Success;
        else return Node.Status.Failure;
    }

    public Node.Status IsItHungry()
    {
        if(hunger <= hungerTreshold && ActivityState == ActionState.Free) return Node.Status.Success;
        else return Node.Status.Failure;
    }

    void IncreaseNeeds()
    {
        float randHunger = Random.Range(5f, 7f) * Time.deltaTime;
        float randThirst = Random.Range(70f, 90f) * Time.deltaTime;
        if (hunger > 0) hunger -= randHunger;
        if (thirst > 0) thirst -= randThirst;
        UI_Manager.OnHealthChanged?.Invoke(-randHunger, -randThirst);
    }
    #endregion

    #region Eating

    public Node.Status Eat()
    {
        if (!isEating)
        {
            // Initialize eating process
            isEating = true;
            ActivityState = ActionState.Busy;
            agent.isStopped = true;
            anim.PlayAnimation(AnimationName.IdleChainsaw);

            var need = maxHunger - hunger;
            var randomChoice = Random.Range(0, need);
            var recover = Mathf.Round(randomChoice / 25) * 25;
            mushroomsToEat = (int)(recover / 25);
            availableFood = Physics.OverlapSphere(transform.position, 40, foodLayer);

            if (mushroomsToEat > availableFood.Length)
            {
                mushroomsToEat = availableFood.Length;
            }

            eatingTime = availableFood[0].GetComponentInParent<IInteractable>().TimeToComplete;
            mushroomsEaten = 0;
        }

        // Check if the eating process is completed
        if (mushroomsEaten >= mushroomsToEat)
        {
            isEating = false;
            agent.isStopped = false;
            ActivityState = ActionState.Free;
            return Node.Status.Success;
        }

        // Continue eating process
        if (eatingTime <= 0)
        {
            var interactable = availableFood[mushroomsEaten].gameObject.GetComponentInParent<IInteractable>();
            interactable?.Interact();
            hunger += 25;
            UI_Manager.OnHealthChanged(25, 0);
            mushroomsEaten++;
            eatingTime = availableFood[0].GetComponentInParent<IInteractable>().TimeToComplete;
        }
        else
        {
            eatingTime -= Time.deltaTime;
        }

        return Node.Status.Running;
    }


    IEnumerator Eating(float _time, Collider[] mushrooms, int mushroomsToEat)
    {

        isEating = true;
        int mushroomsEaten = 0;

        foreach (var m in mushrooms)
        {
            yield return new WaitForSeconds(_time);
            var interactable = m.gameObject.GetComponentInParent<IInteractable>();
            interactable?.Interact();
            hunger += 25;
            UI_Manager.OnHealthChanged(25, 0);
            mushroomsEaten++;
            if (mushroomsEaten >= mushroomsToEat)
            {
                isEating = false;
                agent.isStopped = false;
                yield break;
            }

        }
    }
    #endregion

    #region Drinking

    public Node.Status Drink()
    {

        if (isDrinking)
        {
            agent.isStopped = true;
            ActivityState = ActionState.Busy;
            return Node.Status.Running;
        }

        var need = maxThirst - thirst;
        var randomChoice = Random.Range(100, need);
        var recover = Mathf.Round(randomChoice / 25) * 25;
        var sips = (int)recover / 25;
        StartCoroutine(Drinking(0.5f, sips));
        if (!isDrinking)
        {
            
            ActivityState = ActionState.Free;
            return Node.Status.Success;
        }
        else return Node.Status.Failure;
    }

    IEnumerator Drinking(float _TimePerSip, float _sips)
    {
        isDrinking = true;

        for (var i = 0; i < _sips; i++)
        {
            yield return new WaitForSeconds(_TimePerSip);
            thirst += 25;
            UI_Manager.OnHealthChanged(0, 25);
        }
        isDrinking = false;
        agent.isStopped = false;
    }

    #endregion














    //____________GIZMOS____________\\

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, 40);
    }
}

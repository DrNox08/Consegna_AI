using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decorator : Node
{
    public delegate bool CheckCondition();
    public CheckCondition Function;
   

    public Decorator(string _name, CheckCondition _function)
    {
        name = _name;
        Function = _function;
    }

    public override Status Process()
    {
        Status childStatus = children[currentChild].Process();
        if (Function()) return children[currentChild].Process();
        else return Status.Failure;

        
    }
}

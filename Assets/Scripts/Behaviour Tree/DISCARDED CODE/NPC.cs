
using System.Collections;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class NPC : MonoBehaviour
{
    Root tree;
    Node.Status treeStatus;
    NavMeshAgent agent;
    AnimationController anim;
    bool isDayTime; // not a state
    bool isEating;
    bool isDrinking;
    bool isSleeping;


    Transform nextDestination;
    IInteractable interactable;

    [Header("Health")]
    [SerializeField] float hunger;
    [SerializeField] float thirst;
    [SerializeField] float hungerTreshold;
    [SerializeField] float thirstTreshold;
    float maxHunger = 1000;
    float maxThirst = 1000;
    [Space]
    [Header("Parameters")]
    [SerializeField] float minDistanceToTarget;
    [SerializeField] LayerMask treesLayer;
    [SerializeField] LayerMask foodLayer;
    [Header("Mesh")]
    [SerializeField] GameObject model;
    [SerializeField] GameObject wood;



    [Header("Test")]
    [SerializeField] Transform work;
    [SerializeField] Transform storage;
    [SerializeField] Transform sleep;
    [SerializeField] Transform eat;
    [SerializeField] Transform drink;
    [SerializeField] Transform bedPosition;



    enum ActionState { Free, Busy }
    ActionState ActivityState;

    enum HealthState { Hungry, Thirsty }
    HealthState health;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<AnimationController>();
    }

    private void OnEnable()
    {
        TimeManager.OnTimeChanged += CheckTime;
    }
    private void OnDisable()
    {
        TimeManager.OnTimeChanged -= CheckTime;
    }

    private void Start()
    {
        //Set Health
        hunger = 1000;
        thirst = 1000;

        wood.SetActive(false);

        //Build The Tree
        tree = new Root();
        Node isInNeeds = new ConditionalSelector("Is In Needs?", IsInNeeds);
        Node isDayOrNight = new ConditionalSelector("Is It Day Time?", () => isDayTime);
        Node healthPriority = new ConditionalSelector("What's the need?", IsItThirsty);
        Node goToWork = new Leaf("Go to work", GoToWork);
        Node goToEat = new Leaf("Go To Eat", GoToEat);
        Node goToDrink = new Leaf("Go To Drink", GoToDrink);
        Node goToSleep = new Leaf("Go To Sleep", GoToSleep);
        Node sleep = new Leaf("Sleep", Sleep);
        Node findTree = new Leaf("Find a tree", ChooseTree);
        Node cutTheTree = new Leaf("Cut The Tree", CutTree);
        Node eat = new Leaf("Eating", Eat);
        Node goToTheStorage = new Leaf("Going To The Storage", GoToTheStorage);
        Node drink = new Leaf("Drinking", Drink);
        Node dropWood = new Leaf("Dropping Wood", DropWood);
        Node workRoutine = new Sequence("Working Routine");
        Node eating = new Sequence("Eating Time");
        Node drinking = new Sequence("Drinking Time");
        Node sleeping = new ConditionalSelector("Sleeping Time", () => isSleeping);

        tree.AddChild(isDayOrNight);
        isDayOrNight.AddChild(isInNeeds);
        isDayOrNight.AddChild(sleeping);

        isInNeeds.AddChild(healthPriority);
        isInNeeds.AddChild(workRoutine);

        workRoutine.AddChild(goToWork);
        workRoutine.AddChild(findTree);
        workRoutine.AddChild(cutTheTree);
        workRoutine.AddChild(goToTheStorage);
        workRoutine.AddChild(dropWood);

        healthPriority.AddChild(drinking);
        healthPriority.AddChild(eating);

        drinking.AddChild(goToDrink);
        drinking.AddChild(drink);

        eating.AddChild(goToEat);
        eating.AddChild(eat);

        sleeping.AddChild(goToSleep);
        sleeping.AddChild(sleep);

        ActivityState = ActionState.Free;

        tree.PrintTree();
        isDayTime = true;
    }











    private void Update()
    {
        Debug.Log(treeStatus);
        treeStatus = tree.Process();
        if (treeStatus == Node.Status.Failure || treeStatus == Node.Status.Success) tree.currentChild = 0;


        //if (treeStatus == Node.Status.Success || treeStatus == Node.Status.Failure)
        //{
        //    treeStatus = Node.Status.Running;
        //}


        //if (treeStatus == Node.Status.Running)
        //{
        //    treeStatus = tree.Process();
        //    //Debug.Log("Tree Status: " + treeStatus);
        //}

    }




    //_______________________FUNCTIONS__________________________


    void CheckTime(float value)
    {
        bool actuallyDay = value >= 8 && value <= 23;
        if (actuallyDay) isDayTime = true;
        else if (!actuallyDay && ActivityState == ActionState.Busy) isDayTime = true;
        else if (!actuallyDay && ActivityState == ActionState.Free) isDayTime = false;
        //SleepCheck();
    }

    //________Move Functions________\\
    #region Movement
    Node.Status GoToDestination(Vector3 targetPos)
    {
        float distanceToTarget = Vector3.Distance(targetPos, transform.position);

        
            
                agent.SetDestination(targetPos);
            
            anim.PlayAnimation(AnimationName.WalkChainsaw);
        

        if (distanceToTarget <= minDistanceToTarget)
        {
            
            
            //nextDestination = null;
            return Node.Status.Success;
        }
        return Node.Status.Running;

    }

    public Node.Status GoToWork()
    {
        
            return GoToDestination(work.position);
        
    }


    public Node.Status GoToSleep()
    {
        Debug.Log("Going to sleep");
        

        return GoToDestination(sleep.position);

    }

    public Node.Status GoToEat()
    {
        Debug.Log("Going to eat");
        return GoToDestination(eat.position);
    }

    public Node.Status GoToDrink()
    {

        Debug.Log("Going to drink");
        return GoToDestination(drink.position);
    }

    #endregion


    //__________Actions-> Work Sequence__________\\
    #region Working
    public Node.Status ChooseTree()
    {
        

        if (interactable == null)
        {
            var trees = Physics.OverlapSphere(transform.position, 40, treesLayer);
            int randomTree = Random.Range(0, trees.Length);
            nextDestination = trees[randomTree].gameObject.transform;
            interactable ??= trees[randomTree].gameObject.GetComponentInParent<IInteractable>();
            return Node.Status.Running;
        }

        return GoToDestination(nextDestination.transform.position);


    }

    public Node.Status CutTree()
    {

        IncreaseNeeds();
        interactable.Interact();
        if (interactable.HasBeenCompleted)
        {
            interactable = null;
            return Node.Status.Success;
        }

        else return Node.Status.Running;


    }

    public Node.Status GoToTheStorage()
    {
        wood.SetActive(true);
        IncreaseNeeds();
        nextDestination = storage;
        return GoToDestination(storage.position);
    }

    public Node.Status DropWood()
    {
        if (wood.activeInHierarchy) wood.SetActive(false);
        ActivityState = ActionState.Free;
        return Node.Status.Success;
    }

    #endregion

    //__________Actions-> Sleep Sequence__________\\
    #region Sleeping

    public Node.Status Sleep()
    {
        SleepCheck();

        return Node.Status.Success;
    }

    void SleepCheck()
    {
        if (!isSleeping && !isDayTime)
        {
            model.SetActive(false);
            SleepModel.OnChange();
            isSleeping = true;
        }
        else if (isSleeping && isDayTime)
        {
            model.SetActive(true);
            SleepModel.OnChange();
            isSleeping = false;
        }
    }

    #endregion







    //________Health Functions________\\
    #region Health
    bool IsInNeeds()
    {
        bool realNeed = hunger <= hungerTreshold || thirst <= thirstTreshold || isEating || isDrinking;
        if (realNeed && ActivityState == ActionState.Free) return true;
        if (realNeed && ActivityState == ActionState.Busy) return false; 
        return false;
    }
    public bool IsItThirsty()
    {
        return thirst <= hunger;
    }

    void IncreaseNeeds()
    {
        float randHunger = Random.Range(5f, 7f) * Time.deltaTime;
        float randThirst = Random.Range(7f, 9f) * Time.deltaTime;
        if (hunger > 0) hunger -= randHunger;
        if (thirst > 0) thirst -= randThirst;
        UI_Manager.OnHealthChanged?.Invoke(-randHunger, -randThirst);
    }
    #endregion

    #region Eating

    public Node.Status Eat()
    {
        if (isEating)
        {
            ActivityState = ActionState.Busy;
            anim.PlayAnimation(AnimationName.IdleChainsaw);
            return Node.Status.Running;
        }

        var need = maxHunger - hunger;
        var randomChoice = Random.Range(0, need);
        var recover = Mathf.Round(randomChoice / 25) * 25;
        var mushroomsToEat = (int)(recover / 25);
        var availableFood = Physics.OverlapSphere(transform.position, 40, foodLayer);
        if (mushroomsToEat > availableFood.Length) mushroomsToEat -= mushroomsToEat - availableFood.Length;
        var timeToEatOne = availableFood[0].GetComponentInParent<IInteractable>().TimeToComplete;
        StartCoroutine(Eating(timeToEatOne, availableFood, mushroomsToEat));

        if (!isEating)
        {
            ActivityState = ActionState.Free;
            Debug.Log("ho finito di mangiare");
            return Node.Status.Success;
        }
        return Node.Status.Running;

    }


    IEnumerator Eating(float _time, Collider[] mushrooms, int mushroomsToEat)
    {

        isEating = true;
        int mushroomsEaten = 0;

        foreach (var m in mushrooms)
        {
            yield return new WaitForSeconds(_time);
            var interactable = m.gameObject.GetComponentInParent<IInteractable>();
            interactable?.Interact();
            hunger += 25;
            UI_Manager.OnHealthChanged(25, 0);
            mushroomsEaten++;
            if (mushroomsEaten >= mushroomsToEat)
            {
                isEating = false;
                yield break;
            }

        }
    }
    #endregion

    #region Drinking

    public Node.Status Drink()
    {

        if (isDrinking)
        {
            ActivityState = ActionState.Busy;
            return Node.Status.Running;
        }

        var need = maxThirst - thirst;
        var randomChoice = Random.Range(100, need);
        var recover = Mathf.Round(randomChoice / 25) * 25;
        var sips = (int)recover / 25;
        StartCoroutine(Drinking(0.5f, sips));
        if (!isDrinking)
        {
            ActivityState = ActionState.Free;
            return Node.Status.Success;
        }
        else return Node.Status.Failure;
    }

    IEnumerator Drinking(float _TimePerSip, float _sips)
    {
        isDrinking = true;

        for (var i = 0; i < _sips; i++)
        {
            yield return new WaitForSeconds(_TimePerSip);
            thirst += 25;
            UI_Manager.OnHealthChanged(0, 25);
        }
        isDrinking = false;

    }

    #endregion














    //____________GIZMOS____________\\

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, 40);
    }




}

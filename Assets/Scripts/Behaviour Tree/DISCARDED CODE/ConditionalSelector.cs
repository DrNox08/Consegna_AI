using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionalSelector : Node
{
    public delegate bool CheckCondition();
    public CheckCondition Function;

    bool condition;
    
    public ConditionalSelector(string _name, CheckCondition func)
    {
        name = _name;
        Function = func;
    }



    public override Status Process()
    {

        condition = Function();
        Status childStatus = children[currentChild].Process();

        if (childStatus != Status.Running)
        {
            if (condition) currentChild = 0;
            else currentChild = 1;

        }

        return childStatus;


        
    }



}

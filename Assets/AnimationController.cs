using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    private Animation animationComponent;
    private List<AnimationState> animationStates;

    void Awake()
    {
        // Ottieni il componente Animation dal modello
        animationComponent = GetComponentInChildren<Animation>();

        // Inizializza la lista delle animazioni
        animationStates = new List<AnimationState>();

        // Aggiungi tutte le animazioni dal componente Animation alla lista
        foreach (AnimationState state in animationComponent)
        {
            animationStates.Add(state);
        }
        Debug.Log(animationStates.Count);
    }

    public void PlayAnimation(AnimationName animation)
    {
        int index = (int)animation;

        if (index >= 0 && index < animationStates.Count)
        {
            string animationName = animationStates[index].name;
            
            animationComponent.Play(animationName);
        }
        else
        {
            Debug.LogWarning("Invalid animation index.");
        }
    }
}
public enum AnimationName
{
    IdleHuman,
    WalkHuman,
    Transformation,
    IdleChainsaw,
    WalkChainsaw,
    Attack1,
    Attack2
}
